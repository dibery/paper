MAIN=thesis
LATEX=latexmk -xelatex -f -synctex=1
QUIET=-quiet

only:
	@$(LATEX) $(QUIET) $(MAIN).tex
	@rm -f $(MAIN).xdv

view: only
	@evince $(MAIN).pdf

check:
	@$(LATEX) $(MAIN).tex

clean:
	@rm -f *.aux *.bbl *.blg *.fdb_latexmk *.fls *.log *.pdf *.toc missfont.log *.synctex.gz *.out *.thm *.lof *.lot *.bcf *.run.xml

commit:
	@git add -A Makefile *.tex *.bib image/; \
	read -p "Enter commit message: " MESG; \
	git commit -m "$$MESG"; \
	git push origin master

pull:
	@git pull origin master
